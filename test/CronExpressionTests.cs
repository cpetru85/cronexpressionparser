using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using CronExpressionParser;

namespace CronExpressionParser.Tests
{
    public class CronExpressionTests
    {
        [Fact]
        public void Test1()
        {
            // Arrange
            var sut = new CronExpression("*/15 0 1,15 * 1-5 /usr/bin/find");

            // Act
            var results = sut.GetResults();

            // Assert
            Assert.Equal(results[CronExpressionPartEnum.Minute], new List<int> { 0, 15, 30, 45 });
            Assert.Equal(results[CronExpressionPartEnum.Hour], new List<int> { 0 });
            Assert.Equal(results[CronExpressionPartEnum.DayOfMonth], new List<int> { 1, 15 });
            Assert.Equal(results[CronExpressionPartEnum.Month], Enumerable.Range(1, 12).ToList() );
            Assert.Equal(results[CronExpressionPartEnum.DayOfWeek], Enumerable.Range(1, 5).ToList() );
        }
    }
}
