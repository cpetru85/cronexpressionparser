using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using CronExpressionParser;

namespace CronExpressionParser.Tests
{
    public class CronExpressionPartTests
    {
        [Fact]
        public void When_Expression_Part_Is_Fixed()
        {
            // Arrange
            var sut = new CronExpressionPart(CronExpressionPartRange.GetValidRange(CronExpressionPartEnum.Hour), "17");

            // Act
            var results = sut.GetResults();

            // Assert
            Assert.Equal(results, new List<int> { 17 });
        }

        [Fact]
        public void When_Expression_Part_Is_Wildcard()
        {
            // Arrange
            var sut = new CronExpressionPart(CronExpressionPartRange.GetValidRange(CronExpressionPartEnum.Hour), "*");

            // Act
            var results = sut.GetResults();

            // Assert
            Assert.Equal(results, Enumerable.Range(0, 24).ToList());
        }

        [Fact]
        public void When_Expression_Part_Is_WildcardEveryX()
        {
            // Arrange
            var sut = new CronExpressionPart(CronExpressionPartRange.GetValidRange(CronExpressionPartEnum.Hour), "*/5");

            // Act
            var results = sut.GetResults();

            // Assert
            Assert.Equal(results, new List<int> { 0, 5, 10, 15, 20 });
        }
    }
}
