using System;
using System.Collections.Generic;
using System.Collections;

namespace CronExpressionParser
{
    public class CronExpressionPartRange
    {
        public CronExpressionPartEnum Description { get; private set; }
        public int Min { get; private set; }
        public int Max { get; private set; }

        public CronExpressionPartRange(CronExpressionPartEnum description, int min, int max) {
            this.Description = description;
            this.Min = min;
            this.Max = max;
        }

        public static CronExpressionPartRange GetValidRange(CronExpressionPartEnum part) {
            switch((CronExpressionPartEnum)part) {
                case CronExpressionPartEnum.Minute:
                    return new CronExpressionPartRange(part, 0, 59);
                case CronExpressionPartEnum.Hour:
                    return new CronExpressionPartRange(part, 0, 23);
                case CronExpressionPartEnum.DayOfMonth:
                    return new CronExpressionPartRange(part, 1, 31);
                case CronExpressionPartEnum.Month:
                    return new CronExpressionPartRange(part, 1, 12);
                case CronExpressionPartEnum.DayOfWeek:
                    return new CronExpressionPartRange(part, 1, 7);
                default:
                    throw new Exception("Unrecognized cron expression part type!");
            }
        }
    }
}