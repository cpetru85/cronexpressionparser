using System;

namespace CronExpressionParser
{
    public enum CronExpressionPartEnum {
        Minute = 0,
        Hour,
        DayOfMonth,
        Month,
        DayOfWeek
    }
}