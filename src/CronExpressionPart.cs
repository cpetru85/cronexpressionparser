using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace CronExpressionParser
{
    public class CronExpressionPart
    {
        private readonly int min;
        private readonly int max;
        private readonly string interval;
        private List<int> results;
        private bool validated = false;

        public CronExpressionPartEnum Description { get; private set; }

        public CronExpressionPart(CronExpressionPartRange range, string interval) {
            this.min = range.Min;
            this.max = range.Max;
            this.Description = range.Description;
            this.interval = interval;
            this.results = new List<int>();
        }

        public List<int> GetResults() {
            if(!this.validated) {
                this.Validate();
            }

            return new List<int>(this.results);
        }

        public void PrintResults(Action<string> printer) {
            if(!this.validated) {
                this.Validate();
            }

            printer(this.ToString());
        }

        public override string ToString()
        {
            return $"{this.Description} {string.Join(',', this.results)}";
        }

        public void Validate() {
            this.results = this.Extract();
            this.validated = true;
        }

        private List<int> Extract() {

            Action throwException = () => throw new Exception($"Expression '{interval}' is not valid for '{Description}'!");

            if(string.IsNullOrWhiteSpace(this.interval)) {
                throwException();
            }

            if(interval.Contains("*")) {

                // * 
                if(interval == "*") {

                    // All
                    return this.GetRange(min, max, 1);
                } 

                // */15
                else if(interval.Contains("/")) {
                    
                    // All every X
                    var intervalParts = interval.Split("/");
                    if(intervalParts.Length != 2 || string.IsNullOrWhiteSpace(intervalParts[1])) {
                        throwException();
                    }

                    var intervalStepPart = 0;
                    if(!int.TryParse(intervalParts[1], out intervalStepPart)) {
                        throwException();
                    }

                    if(intervalStepPart < this.min || intervalStepPart > this.max) {
                        throwException();
                    }

                    return this.GetRange(min, max, intervalStepPart);
                } 
                
                // Not valid
                else {
                    throwException();
                }

            } else if(interval.Contains("-")) {

                // 1-5
                // All between X and Y
                var rangeParts = interval.Split("-");
                if(rangeParts.Length != 2 || rangeParts.Any(p => string.IsNullOrWhiteSpace(p))) {
                    throwException();
                }

                var rangeStartPart = 0;
                if(!int.TryParse(rangeParts[0], out rangeStartPart)) {
                    throwException();
                }

                var rangeEndPart = 0;
                if(!int.TryParse(rangeParts[1], out rangeEndPart)) {
                    throwException();
                }

                if(rangeStartPart < this.min || rangeEndPart > this.max) {
                    throwException();
                }
                
                return this.GetRange(rangeStartPart, rangeEndPart, 1);

            } else if(interval.Contains(",")) {

                // 1,15
                // X, Y, (..) only
                var fixedParts = interval.Split(",");
                if(fixedParts.Length < 2 || fixedParts.Any(p => string.IsNullOrWhiteSpace(p))) {
                    throwException();
                }

                if(fixedParts.Any(p => !int.TryParse(p, out _))) {
                    throwException();
                }

                var ret = fixedParts.Select(p => Convert.ToInt32(p)).ToList();

                if(ret.Any(p => p < this.min || p > this.max )) {
                    throwException();
                }

                return ret;

            } else {

                // X only
                var fixedPart = 0;
                if(!int.TryParse(interval, out fixedPart)) {
                    throwException();
                }

                if(fixedPart < this.min || fixedPart > this.max) {
                    throwException();
                }

                return new int[] { fixedPart }.ToList();
            }

            throwException();
            return null;
        }

        private List<int> GetRange(int min, int max, int step) {
            var ret = new List<int>();
            for(var i = min; i <= max; i += step) {
                ret.Add(i);
            }
            return ret;
        }
    }
}