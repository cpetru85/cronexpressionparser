﻿using System;

namespace CronExpressionParser
{
    // 
    // Input
    // */15 0 1,15 * 1-5 /usr/bin/find
    //

    //
    // Output
    // minute          0 15 30 45
    // hour            0
    // day of month    1 15
    // month           1 2 3 4 5 6 7 8 9 10 11 12
    // day of week     1 2 3 4 5
    // command         /usr/bin/find
    //

    // Cron expression format
    // ┌───────────── minute (0 - 59)
    // │ ┌───────────── hour (0 - 23)
    // │ │ ┌───────────── day of the month (1 - 31)
    // │ │ │ ┌───────────── month (1 - 12)
    // │ │ │ │ ┌───────────── day of the week (0 - 6) (Sunday to Saturday; 7 is also Sunday on some systems)
    // │ │ │ │ │                                   
    // │ │ │ │ │
    // │ │ │ │ │
    // * * * * * <command to execute>

    
    class Program
    {
        static int Main(string[] args)
        {
            Console.WriteLine("Cron expression parser running");
            try {
                new CronExpression("*/15 0 1,15 * 1-5 /usr/bin/find").PrintResults(Console.WriteLine);
                return 0;
            } catch(Exception ex) {
                Console.WriteLine($"Exception occured: {ex.Message}");
                return 1;
            }
        }
    }
}
