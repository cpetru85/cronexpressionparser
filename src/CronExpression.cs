using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace CronExpressionParser
{
    public class CronExpression
    {
        private readonly string expression;
        private List<CronExpressionPart> parts;
        private string command;
        private bool validated = false;

        public CronExpression(string expression) {
            this.expression = expression;
            this.parts = new List<CronExpressionPart>();
        }

        public Dictionary<CronExpressionPartEnum, List<int>> GetResults() {
            if(!this.validated) {
                this.Validate();
            }

            return this.parts.ToDictionary(p => p.Description, p => p.GetResults());
        }

        public void PrintResults(Action<string> printer){
            if(!this.validated) {
                this.Validate();
            }

            foreach(var part in this.parts) {
                part.PrintResults(printer);
            }
            printer($"command {this.command}");
        }

        private void Validate() {

            // 
            // Input
            // */15 0 1,15 * 1-5 /usr/bin/find
            //

            //
            // Output
            // minute          0 15 30 45
            // hour            0
            // day of month    1 15
            // month           1 2 3 4 5 6 7 8 9 10 11 12
            // day of week     1 2 3 4 5
            // command         /usr/bin/find
            //

            if(string.IsNullOrWhiteSpace(this.expression)) {
                throw new Exception("Empty cron expression");
            }

            int curr = 0, prev = 0, partIndex = 0, commandIndex = Enum.GetValues(typeof(CronExpressionPartEnum)).Cast<int>().Max();;
            while(curr < this.expression.Length) {

                if(partIndex == commandIndex + 1) {
                    this.command = this.expression.Substring(prev);
                    break;
                }

                if(' ' == this.expression[curr]) {

                    var part = new CronExpressionPart(
                        CronExpressionPartRange.GetValidRange((CronExpressionPartEnum)partIndex++),
                        this.expression.Substring(prev, curr - prev));

                    prev = curr + 1;

                    part.Validate();
                    this.parts.Add(part);
                }
                curr++;
            }
        }
    }
}